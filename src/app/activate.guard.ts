import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class ActivateGuard implements CanActivate {

  constructor(private _us:UserService, private router: Router){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      //no restruction till now!
      console.log('route is running ActivateGuard ')
      //here we need to write logic to check from an service 
      //that service will talk to the backend to get info of un/pw
      //if it is ok it will put userdetails into the brower memoery *

      if(this._us.isAdminRight()){
        return true;
      }else{
        alert('you dont have admin permission');
        this.router.navigate(['home']);
      }
    return true;
  }
  
}
